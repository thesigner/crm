export default [
    //custom

    {
        path: '/projects/display',
        meta: {
            breadcrumb: false
        },
        name: 'projects/display',
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            '@/pages/projects/Projects.vue'
        )
    },
    {
        path: '/projects/display/:project',
        name: 'projects/display',
        meta: {
            breadcrumb: false
        },
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            '@/pages/projects/Project.vue'
        )
    },
    {
        path: '/projects/add',
        meta: {
            breadcrumb: false
        },
        name: 'projects/add',
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            '@/pages/projects/AddProject.vue'
        )
    },
    // custom


    {
        path: '*',
        meta: {
            public: true,
        },
        redirect: {
            path: '/404'
        }
    },
    {
        path: '/404',
        meta: {
            public: true,
        },
        name: 'NotFound',
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            `@/pages/NotFound.vue`
        )
    },
    {
        path: '/403',
        meta: {
            public: true,
        },
        name: 'AccessDenied',
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            `@/pages/Deny.vue`
        )
    },
    {
        path: '/500',
        meta: {
            public: true,
        },
        name: 'ServerError',
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            `@/pages/Error.vue`
        )
    },
    {
        path: '/login',
        meta: {
            public: true,
        },
        name: 'Login',
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            `@/pages/Login.vue`
        )
    },
    {
        path: '/',
        meta: {},
        name: 'Root',
        redirect: {
            name: 'Dashboard'
        }
    },
    {
        path: '/dashboard',
        meta: {
            breadcrumb: false
        },
        name: 'Dashboard',
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            `@/pages/Dashboard.vue`
        )
    },

    {
        path: '/media',
        meta: {},
        name: 'Media',
        props: (route) => ({
            type: route.query.type
        }),
        component: () => import(
            /* webpackChunkName: "routes" */
            /* webpackMode: "lazy-once" */
            `@/pages/Media.vue`
        )
    },
    {
        path: '/chat',
        meta: {
            public: true,
        },
        name: 'Chat',
        component: () => import(
            /* webpackChunkName: "routes" */
            `@/components/chat/ChatLayout.vue`
        ),
        redirect: {
            path: '/chat/messaging'
        },
        children: [{
                path: '/chat/messaging/:uuid?',
                meta: {
                    public: true,
                },
                name: 'ChatMessaging',
                props: true,
                components: {
                    default: () => import(
                        /* webpackChunkName: "routes" */
                        /* webpackMode: "lazy-once" */
                        `@/components/chat/ChatMessaging.vue`
                    ),
                }
            },
            {
                path: '/chat/contact/:uuid?',
                meta: {
                    public: true,
                },
                name: 'ChatContact',
                components: {
                    default: () => import(
                        /* webpackChunkName: "routes" */
                        /* webpackMode: "lazy-once" */
                        `@/components/chat/ChatContact.vue`
                    ),

                }
            }
        ]
    },   
];
