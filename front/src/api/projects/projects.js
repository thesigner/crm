const Items = [{
    'name': 'Dessie',
    'email': 'Dessie7937@gmail.com',
    'username': 'Dessie79',
    'jobTitle': 'Web Developer',
    'phone': '1-360-812-9380 x511',
    'avatar': '/static/avatar/a2.jpg',
}];


const getUserById = (uuid) => {
    return (uuid === undefined) ? Items[0] : Items.find(x => x.uuid === uuid);
};

const getUser = (limit) => {
    return (limit) ? Items.slice(0, limit) : Items;
};

export {
    Items,
    getUser,
    getUserById
};
