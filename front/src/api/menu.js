const Menu = [{
	header: 'Zart/gif na dzis :D'
},
{
	title: 'Dashboard',
	group: 'apps',
	icon: 'dashboard',
	name: 'Dashboard',
},
{
	title: 'Chat',
	group: 'apps',
	icon: 'chat_bubble',
	name: 'Chat',
},
{
	title: 'Drive',
	group: 'apps',
	icon: 'perm_media',
	name: 'Media',
},
{
	title: 'Projekty',
	group: 'projects',
	component: 'projects',
	icon: 'widgets',
	items: [{
			name: 'list',
			title: 'Dodaj',
			component: 'projects/add'
		},
		{
			name: 'list',
			title: 'Wszystkie',
			component: 'projects/display'
		},

	]
},
];


var menu = Menu;

// reorder menu
menu.forEach((item) => {
if (item.items) {
	item.items.sort((x, y) => {
		let textA = x.title.toUpperCase();
		let textB = y.title.toUpperCase();
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
	});
}
});

export default menu;
