<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;

class ProjectsController extends Controller
{
	public function saveProject(Request $request)
	{
		$projests = Projects::create($request->all());
		return;
	}


	public function getProjects()
	{	
		$projects = Projects::all();
		return response()->json($projects);
	}

	public function getProject(Request $request)
	{	
		$project = Projects::find($request->input('project'));
		return response()->json($project);
	}

	public function deleteProject(Request $request)
	{
		$project = Projects::find($request->input('project'));
		$project->delete();
		return;
	}
}
