# Env requirements:
1. domena http://crm.io/ - [Poradnik jak to zrobić](https://gist.github.com/omarusman/5530016)
2. composer (tylko do backendu)
3. php 7.0^
3. node
4. npm

# Front env
```bash
>> dev
cd front
npm run dev

>> prod
npm run build
```

# Back env
```bash
>> crm.io/
>> crm.io/api
```

# Git
- Każdy plik komentujemy i commitujemy osobno, chyba, że wdrażamy kilka zależnych od siebie plików.


# Stylistyka komentarzy
```js
/**
 * Get Project
 * @param {Number} id ID
 * @param {Object} args Arguments
 * @return {JSON} Project
 */
function getProject(id, args) {
    return {
    	'project': 'name'
    };
}
```


# TODO ✅❌
- ✅ Boilerplate front-endu vue + [Vetifyjs](https://vuetifyjs.com/en/getting-started/quick-start) (gotowe komponenty)
- ✅ Boilerplate back-endu [Laravel](https://laravel.com/docs/5.7)
- ✅ Auth backendu
- ✅ Repozytorium [https://bitbucket.org/thesigner/crm](https://bitbucket.org/thesigner/crm)
- ✅ .gitignore
- ❌ Określić formę pracy
- ❌ Zebrać listę oczekiwań i pomysłów
- ❌ Zebrać listę usterek w obecnym CRM
- ❌ Zebrać listę prywatnych preferencji użytkowania
- ❌ Określić ramy czasowe
- ❌ Określić podział prac
- ❌ Wdrożyć zespół w strukturę projektu
- ❌ Stworzyć priorytet wdrażania koponentów
- ❌ Stworzyć listę endpointów
- ❌ Stworzyć strukturę crud
- ❌ Stworzyć listę globalnych funkcji front-endowych (dostępy do funkcji z każdego miejsca)
- ❌ Makieta projektu
- ❌ UX


